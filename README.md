# Scorelattes Lote

**scorelattes em lote, dado um arquivo de entrada**

**adaptado de scorelattes <https://vicentehelano.github.io/scoreLattes/>**

## Requisitos

* [Docker](https://docs.docker.com/get-docker)
* [Docker-compose](https://docs.docker.com/compose/install/)

## Entrada

* Arquivo CSV com as colunas: idlattes,nome,area capes e unidade academica. Exemplo abaixo:

```
idlattes;nome;area;ua
9099116287225122;SERVIDOR 1;ENGENHARIAS I;CCT
5116712500151643;SERVIDOR 2;ARTES;IISCA
9245122165772401;SERVIDOR 3;CIENCIA_DA_COMPUTACAO; CCT
```

## Saída

* output/resultados.csv

## Como executar

```

docker-compose run --rm python2 python /python/scorerun.py

```

## Barema

* <https://docs.google.com/document/d/1b1nly8IpPnCvmPir7JHyzLVNn3i8fvoY4V4vLhOMqMw/edit?usp=sharing>

## Autor

* Prof. Rafael Perazzo Barbosa Mota ( rafael.mota (at) ufca.edu.br )

> Plataforma Scorelattes em lote
>    Copyright (C) 2021  Rafael Perazzo Barbosa Mota
>
>   This program is free software: you can redistribute it and/or modify
>    it under the terms of the GNU General Public License as published by
>    the Free Software Foundation, either version 3 of the License, or
>    (at your option) any later version.
>
>    This program is distributed in the hope that it will be useful,
>    but WITHOUT ANY WARRANTY; without even the implied warranty of
>    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
>    GNU General Public License for more details.
>
>    You should have received a copy of the GNU General Public License
>    along with this program.  If not, see <https://www.gnu.org/licenses/>.
