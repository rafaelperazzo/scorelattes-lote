FROM python:3
ENV TZ=America/Fortaleza
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
RUN apt-get update
RUN pip install numpy lxml unidecode requests
RUN pip install bs4
RUN pip install progressbar2
RUN pip install pandas odfpy openpyxl
